# Outlook Phishing Addon
## Description 
This is an addon for Outlook using the Outlook Add-in API requirement set 1.3. It is written in HTML and JavaScript and can be packaged up for publishing on your local Exchange server. 

The addon is designed to take the currently opened/active email from Outlook, extract some information (subject, from/to, time delivered), and attach the email and info to a new email. The new email is then sent to an address specified in your orgnaization that is monitored for phishing complaints by your Information Security Department. 

## License
This software is licensed under GNU General Public License v3.0. You can read the full license in license.md, or at [http://www.gnu.org/licenses/gpl-3.0.txt]. 